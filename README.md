Projekat u okviru kursa Alati za razvoj softvera

Link ka originalnom projektu na kome su alati primenjeni: https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2020-2021/12-nexus-defense

Primenjeni alati:
    1. Git
    2. Git hooks
    3. Clang-tidy
    4. Clang-format
    5. CMake
    6. Staticki analizatori: Cppcheck, Clazy
    7. Valgrind
    8. Doxygen
    9. Gitlab CI
    10. Gcov
    11. GDB

_Napomena: Docker nije primenjen zbog problema sa drajverima._
